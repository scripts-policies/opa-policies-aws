package terraform.policy.ec2.analysis

#List of all resources of a given type
resources[resource_type] = all {
    some resource_type
    resource_types[resource_type]
    all := [name |
            name := input.resource_changes[_]
            name.type == resource_type
    ]
}

# number of creations of resources of a given type
creates[resource_type] = creates {
        some resource_type
        resource_types[resource_type]
        all := resources[resource_type]
        creates := [res | res := all[_]; res.change.actions[_] == "create"]
}

resource_types = {"aws_instance"}

allowed_instance_types = {"t2.micro", "t2.small", "t2.medium"}

# list of all resources of a given type

ensure_instance_types[msg] {
        some i
        instance := creates[_][i].change.after
        not is_allowed_instance_type(instance)
        msg := sprintf("EC2 is not using one of the accepted instance types. Accepted values are %v", [allowed_instance_types])
}

is_allowed_instance_type(instance) {
        some i
        instance_type := allowed_instance_types[i]
        instance_type == instance.instance_type
}

default allow = false

allow {
        count(ensure_instance_types) == 0
}

allow_detailed[msg] {
        msg := {
                "allow": allow,
                "ensure_instance_types": ensure_instance_types,
        }
}