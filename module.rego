package terraform.policy.modules.analysis
# opa eval --data modules.rego --input test/modules_01/plan.json 'data.terraform.policy.modules.analysis.allow_detailed'
import input as tfplan

flaggable_provisioners = ["local-exec"]
flaggable_resource_types = ["null_resource"]
allowed_source_repos = ["ricardocg-tf-aws"]
restricted_resource_types = {"aws_instance", "aws_s3_bucket"}

default allow = false

allow {
allow_detailed.valid == true
}

allow_detailed = msg {
nrmc := null_resource_module_calls
nrv := null_resource_violation
emc := ensure_module_location
erum := ensure_resources_use_modules
nm := nested_module
findings := count(nrv) + count(nrmc) + count(emc) + count(erum) + count(nm)
msg := {
"valid": findings == 0,
"violation": ((nrmc | nrv) | emc) | (erum | nm),
}
}

null_resource_violation[explanation] {
resources := tfplan.configuration.root_module.resources[_]
resources.address == null_resource_changes[_].address
provisioners := resources.provisioners[_]
provisioners.type == flaggable_provisioners[_]
explanation := sprintf("Detected usage of local-exec on %v", [resources.address])
}

null_resource_changes[changes] {
changes := tfplan.resource_changes[_]
changes.type == flaggable_resource_types[_]
}

null_resource_module_calls[explanation] {
some key
mc := tfplan.configuration.root_module.module_calls[key]
mc.module.resources[_].type == flaggable_resource_types[_]
mc.module.resources[_].provisioners[_].type == flaggable_provisioners[_]
repo := allowed_source_repos[_]
not contains(mc.source, repo)
some i
address := mc.module.resources[i].address
address_to_compare := sprintf("module.%v.%v", [key, address])
address_to_compare == null_resource_changes[_].address
explanation := sprintf("Detected usage of local-exec on %v", [address_to_compare])
}

ensure_resources_use_modules[explanation] {
some i
resource := root_resources[_][i]
is_one_of_restricted_resources(resource)
explanation := sprintf("Resource is one of restricted resource that only can be used using module - %v", [resource.type])
}

root_resources[resource_type] = all {
some resource_type
some i
restricted_resource_types[resource_type]
all := [name |
name := tfplan.configuration.root_module.resources[i]
name.type == resource_type
]
}

# number of creations of resources of a given type
all_modules[resource_type] = all {
some resource_type
some i
restricted_resource_types[resource_type]
all := [name |
name := tfplan.configuration.root_module.module_calls[i]
]
}

is_one_of_restricted_resources(resource) {
some i
resource_type := restricted_resource_types[i]
resource_type == resource.type
}

######################################################
# Rule: Enforce policy that modules are sourced only from
#        ricardocg-tf-aws gitlab group. Unless 
#          they are submodules in the project.
######################################################

ensure_module_location[explanation] {
some i
module := all_modules[_][i]
not is_module_from_grp_ops_cloudauto(module)
not is_module_nested(module)
explanation := sprintf("Module is not sourced from grp-ops-cloudauto gitlab project - %v", [module.source])
}

is_module_from_grp_ops_cloudauto(module) {
contains(module.source, "ricardocg-tf-aws")
}

nested_module[explanation] {
module := all_modules[_][i]
is_module_nested(module)
explanation := sprintf("WARNING: Module could contain restricted resources - %v", [module.source])
}

 is_module_nested(module) {
contains(module.source, "./")
 }